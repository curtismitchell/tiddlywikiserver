# README

MPTWServer is a Monkey Pirate Tiddly Wiki server for creating, sharing, and storing TiddlyWiki files.

## What is a TiddlyWiki?
A TiddlyWiki is a (now old-school-cool) single page application made from Html, CSS, and Javascript (pre-JQuery).  It is intended to be a personal wiki for keeping notes or organizing information in a pretty cool way.  The original TiddlyWiki can be found here: [http://www.tiddlywiki.com/]()

The variation used here, Monkey Pirate Tiddly Wiki, can be found here: [http://mptw.tiddlyspot.com/]()

