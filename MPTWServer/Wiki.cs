﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MPTWServer
{
    class Wiki
    {
        public Wiki(string slug)
        {
            LoadDefaultContent();
            this.Content = this.Content.Replace("[SAVE_URL_GOES_HERE]", String.Concat("/wikis/", slug));
            this.Slug = slug;
        }

        public string Slug { get; private set; }
        public string Content { get; set; }
        
        private void LoadDefaultContent()
        {
            this.Content = global::MPTWServer.Properties.Resources.DefaultContent;
        }
    }
}
