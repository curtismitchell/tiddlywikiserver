﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Hosting.Self;
using Raven.Client.Embedded;

namespace MPTWServer
{
    class Program
    {
        const int Port = 9001;
        const string UriPattern = "http://localhost:{0}";

        static void Main(string[] args)
        {
            Console.WriteLine("Initializing database...");
            InitializeDb();

            var uri = new Uri(String.Format(UriPattern, Port));
            var host = new NancyHost(uri);
            host.Start();

            Console.WriteLine("MPTW Server Listening @ {0}", uri.ToString());
            Console.WriteLine("Hit [Enter] to exit...");
            Console.ReadLine();
            
            host.Stop();
        }

        private static void InitializeDb()
        {
            var w = new WikiRepository();
            w.Initialize();
        }
    }

    public class WikiModule : NancyModule
    {
        WikiRepository _repo;

        public WikiModule() : base("/wikis")
        {
            _repo = new WikiRepository();

            Get["/{name}"] = parameters =>
            {
                var name = GetSluggifiedName(parameters.name);
                var wiki = _repo.ReturnWikiNamed(name);

                return wiki.Content;
            };

            Post["/{name}"] = parameters =>
            {
                var name = GetSluggifiedName(parameters.name);
                var wiki = _repo.ReturnWikiNamed(name);
                wiki.Content = this.Request.Form.wiki;

                if (String.IsNullOrEmpty(wiki.Content))
                    return "Save failed.";

                _repo.SaveWiki(wiki);
                return "Saved successfully";
            };
        }

        private string GetSluggifiedName(string name)
        {
            // sluggified means alphanumeric (not enforced yet)
            // no spaces
            return name.Replace(' ', '_');
        }
    }

    class WikiRepository
    {
        private string _dataDirectory = @"~\Data";
        private static EmbeddableDocumentStore docStore;

        public WikiRepository()
        {
            CreateDocStoreInstance();
        }

        public void Initialize()
        {
            try
            {
                docStore.Initialize();
            }
            catch (InvalidOperationException) 
            {
                CreateDocStoreInstance();
            }
        }

        private void CreateDocStoreInstance()
        {
            if (docStore != null) return;
            docStore = new EmbeddableDocumentStore() { DataDirectory = _dataDirectory };
        }

        public bool SaveWiki(Wiki wiki)
        {
            using (var session = docStore.OpenSession())
            {             
                session.Store(wiki);
                session.SaveChanges();
                return true;
            }
        }

        public Wiki ReturnWikiNamed(string name)
        {
            Wiki wiki;
            using (var session = docStore.OpenSession())
            {
                wiki = session.Query<Wiki>()
                    .Where(w => w.Slug.Equals(name, StringComparison.InvariantCulture))
                    .FirstOrDefault();
            }
            return (wiki == null) ? new Wiki(name) : wiki;
        }
    }
}
